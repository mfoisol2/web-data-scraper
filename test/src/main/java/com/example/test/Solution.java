package com.example.test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import com.opencsv.CSVWriter;

/**
 * Fetches data from website and saves to CSV file.
 */
public class Solution {

	private static final String FILE_NAME = "CBOE.csv";
	private static final String FILE_PATH = "src//main//resources//";
	private static final String URL = "https://finance.yahoo.com/quote/%5EVIX/history?p=%5EVIX";
	
    public static void main( String[] args ) {
    	getAndSaveData();
	}
    
    /**
     * Retrieves data from website and saves to CSV file.
     * 
     * @throws RuntimeException if table is empty
     */
    private static void getAndSaveData() throws RuntimeException {
    	// running in another thread to keep main UI free
    	Thread t = new Thread() {
			 public void run() {
				Document doc = null;
				try {
					// getting the html document
					doc = Jsoup.connect(URL).get();	
				} catch (IOException e) {
					e.printStackTrace();
				}
					
				// get the table
				Element table = doc.select("table").get(0);
				if (table == null) {
					throw new RuntimeException("No data found to be inserted!");
				}
				
				// get the rows
				Elements rows = table.select("tr");
				ArrayList<String[]> dataToBeSaved = new ArrayList<String[]>();
				Elements beginingCols = rows.select("th");
				
				StringBuilder values = new StringBuilder();
				for (Element col: beginingCols) {
					values.append(col.text()).append('/');
				}
				dataToBeSaved.add(values.toString().split("/"));
				System.out.println(values.toString());
				for (Element row: rows) {
					values = new StringBuilder();
					Elements cols = row.select("td");
					if (cols.size() > 1) {
						for (Element col: cols) {
							values.append(col.text()).append('/');
						}
						dataToBeSaved.add(values.toString().split("/"));
						System.out.println(values.toString());
					}
				 }
				
				writeToCsv(dataToBeSaved);
			 }
		};

		t.start();
    }

	/**
	 * Writes website table data to CBOE file.
	 * 
	 * @param data list of data to be saved into file
	 */
	private static void writeToCsv(ArrayList<String[]> data) {

		try {
			FileOutputStream fos = new FileOutputStream(FILE_PATH + FILE_NAME);
			OutputStreamWriter osw = new OutputStreamWriter(fos, StandardCharsets.UTF_8);
			CSVWriter writer = new CSVWriter(osw);
			writer.writeAll(data);
			writer.close();
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}
}
